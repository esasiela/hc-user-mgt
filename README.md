# Hedge Court User Management

[![pipeline status](https://gitlab.com/esasiela/hc-user-mgt/badges/master/pipeline.svg)](https://gitlab.com/esasiela/hc-user-mgt/commits/master)

This is the repository for the user management web application for Hedge Court, written in Python / Flask / React.

At the moment, authentication and login/logout are also performed in this application, with future plans to move auth to a separate service.
