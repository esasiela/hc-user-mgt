#!/bin/sh

echo "Hedge Court (entrypoint.sh): Waiting for PostgreSQL..."

while ! nc -z users-db 5432; do
  sleep 0.1
done

echo "Hedge Court (entrypoint.sh): PostgreSQL started."

echo "Hedge Court (entrypoint.sh): starting Flask app manage.py..."

python manage.py run -h 0.0.0.0
