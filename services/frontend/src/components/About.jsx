import React from "react";

const About = () => (
  <div>
    <h1 className="title is-1">About</h1>
    <hr />
    <br />
    <p className="content">
      This application automatically deploys from GitLab to Heroku if the build
      passes all tests.
    </p>
  </div>
);

export default About;
