import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Modal from "react-modal";
import axios from "axios";

import About from "./components/About";
import Message from "./components/Message";
/*
import UsersList from "./components/UsersList";
import NavBar from "./components/NavBar";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";
import UserStatus from "./components/UserStatus";

import AddUser from "./components/AddUser";
*/

const modalStyles = {
  content: {
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    border: 0,
    background: "transparent"
  }
};

Modal.setAppElement(document.getElementById("root"));

class App extends Component {
  constructor() {
    super();

    this.state = {
      ping: "ping",
      users: [],
      title: "Hedge Court",
      accessToken: null,
      messageType: null,
      messageText: null,
      showModal: false
    };
  }

  componentDidMount = () => {
    this.getPing();
  };

  getPing = () => {
    console.log(
      "getPing() running...[" +
        `${process.env.REACT_APP_HC_USER_SERVICE_URL}` +
        "]"
    );
    axios
      .get(`${process.env.REACT_APP_HC_USER_SERVICE_URL}/ping`)
      .then(res => {
        console.log(res.data);
        this.setState({ ping: res.data.message });
        this.createMessage("success", res.data.message);
      })
      .catch(err => {
        console.log(err);
      });
    console.log("getPing() done");
  };

  createMessage = (type, text) => {
    this.setState({
      messageType: type,
      messageText: text
    });

    setTimeout(() => {
      this.removeMessage();
    }, 3000);
  };

  removeMessage = () => {
    this.setState({
      messageType: null,
      messageText: null
    });
  };

  render() {
    return (
      <div>
        <div className="container">
          {this.state.messageType && this.state.messageText && (
            <Message
              messageType={this.state.messageType}
              messageText={this.state.messageText}
              removeMessage={this.removeMessage}
            />
          )}
          <div className="columns">
            <div className="column is-half">
              <br />
              <Switch>
                <Route
                  exact
                  path="/"
                  render={() => (
                    <div>
                      <h1 className="title is-1 is-1">Hedge Court</h1>
                      <hr />
                      <br />
                      <p>{this.state.ping}</p>
                      <button
                        onClick={this.getPing}
                        className="button is-primary"
                      >
                        Update Ping
                      </button>
                    </div>
                  )}
                />
                <Route exact path="/about" component={About} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
