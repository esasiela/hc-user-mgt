@ECHO OFF

SET PYTEST_FAIL=0
docker-compose exec users-backend python -m pytest "project/tests" --cov=project -rf
IF %ERRORLEVEL% GTR 0 (
   SET PYTEST_FAIL=1
)

ECHO flake8 (python linting)...
SET FLAKE_FAIL=0
docker-compose exec users-backend python -m flake8 manage.py project
IF %ERRORLEVEL% GTR 0 (
  ECHO flake8 failed on at least one check
  SET FLAKE_FAIL=1
)

ECHO black (python format static analysis)...
SET BLACK_FAIL=0
docker-compose exec users-backend python -m black --exclude=migrations --check manage.py project
IF %ERRORLEVEL% GTR 0 (
  ECHO black failed on at least one check
  SET BLACK_FAIL=1
)

ECHO isort (import formatting)...
SET ISORT_FAIL=0
REM docker-compose exec users-backend /bin/sh -c "isort --check-only manage.py project/*.py project/**/*.py"
docker-compose exec users-backend python -m isort --check-only -rc .
IF %ERRORLEVEL% GTR 0 (
  ECHO isort failed on at least one check
  SET ISORT_FAIL=1
)

ECHO prettier (react formatting)...
SET PRETTIER_FAIL=0
docker-compose exec users-frontend npm run prettier:check
IF %ERRORLEVEL% GTR 0 (
  ECHO prettier failed
  SET PRETTIER_FAIL=1
)

ECHO eslint (react linting)...
SET ESLINT_FAIL=0
docker-compose exec users-frontend npm run lint
IF %ERRORLEVEL% GTR 0 (
  ECHO eslint failed
  SET ESLINT_FAIL=1
)

ECHO jest (react tests)...
SET JEST_FAIL=0
docker-compose exec users-frontend npm run test -- --watchAll=false
IF %ERRORLEVEL% GTR 0 (
  ECHO jest failed
  SET JEST_FAIL=1
)

ECHO/
ECHO Summary:

SET RETURN_CODE=0

IF %PYTEST_FAIL%==0 (
  ECHO PASS: pytest passed all checks
) ELSE (
  ECHO FAIL: pytest failed at least one check
  ECHO       examine output and manually fix
  SET RETURN_CODE=1
)

IF %FLAKE_FAIL%==0 (
  ECHO PASS: flake8 passed all checks
) ELSE (
  ECHO FAIL: flake8 failed at least one check
  ECHO       examine output and manually fix
  SET RETURN_CODE=1
)

IF %BLACK_FAIL%==0 (
  ECHO PASS: black passed all checks
) ELSE (
  ECHO FAIL: black failed at least one check
  ECHO       docker-compose exec users-backend python -m black --exclude=migrations manage.py project
  SET RETURN_CODE=1
)

IF %ISORT_FAIL%==0 (
  ECHO PASS: isort passed all checks
) ELSE (
  ECHO FAIL: isort failed at least one check
  ECHO       docker-compose exec users-backend python -m isort -rc .
REM  ECHO       docker-compose exec users-backend /bin/sh -c "isort --check-only manage.py project/*.py project/**/*.py"
REM  ECHO       docker-compose exec users-backend /bin/sh -c "isort manage.py"
REM  ECHO       docker-compose exec users-backend /bin/sh -c "isort project/*.py"
REM  ECHO       docker-compose exec users-backend /bin/sh -c "isort project/**/*.py"
  SET RETURN_CODE=1
)

IF %PRETTIER_FAIL%==0 (
  ECHO PASS: prettier passed all checks
) ELSE (
  ECHO FAIL: prettier failed at least one check
  ECHO       docker-compose exec users-frontend npm run prettier:write
  SET RETURN_CODE=1
)

IF %ESLINT_FAIL%==0 (
  ECHO PASS: eslint passed all checks
) ELSE (
  ECHO FAIL: eslint failed at least one check
  ECHO       docker-compose exec users-frontend npm run lint -- --fix
  SET RETURN_CODE=1
)

IF %JEST_FAIL%==0 (
  ECHO PASS: jest passed all checks
) ELSE (
  ECHO FAIL: jest failed at least one check
  ECHO       docker-compose exec users-frontend npm run test -- --watchAll=false
  SET RETURN_CODE=1
)


ECHO/
IF %RETURN_CODE%==0 (
  ECHO PASS: all checks passed
) ELSE (
  ECHO FAIL: at least one check failed
)

EXIT /B %RETURN_CODE%
